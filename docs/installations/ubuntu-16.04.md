### How to Install Go 1.6 on Ubuntu 16.04
#### Step 1 — Installing Go

Visit the official Go downloads page and find the URL for the current binary release's tarball, along with its SHA256 hash. Make sure you're in your home directory, and use curl to retrieve the tarball:
```
cd ~
curl -O https://dl.google.com/go/go1.9.4.linux-amd64.tar.gz
```

Next, use tar to extract the tarball. The x flag tells tar to extract, v tells it we want verbose output (a listing of the files being extracted), and f tells it we'll specify a filename:
```
tar xvf go1.9.4.linux-amd64.tar.gz
```

You should now have a directory called go in your home directory. Recursively change go's owner and group to root, and move it to /usr/local:
```
sudo chown -R root:root ./go
sudo mv go /usr/local

```

````
Note: Although /usr/local/go is the officially-recommended location, some users may prefer or require different paths.

````

#### Step 2 — Setting Go Paths
In this step, we'll set some paths in your environment.
First, set Go's root value, which tells Go where to look for its files.
```
sudo nano ~/.profile
```

At the end of the file, add this line:
```
export GOPATH=$HOME/work
export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin
```

If you chose an alternate installation location for Go, add these lines instead to the same file. This example shows the commands if Go is installed in your home directory:
```
export GOROOT=$HOME/go
export GOPATH=$HOME/work
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
```

With the appropriate line pasted into your profile, save and close the file. Next, refresh your profile by running:
```
source ~/.profile
```
